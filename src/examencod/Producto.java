/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencod;

/**
 * La clase Producto hace referencia a los productos de una tienda, de los cuales almacenamos nombre y precio.
 * @author ebarreirosuris
 */
public class Producto {
    
    private String nombre;
    private double precio;
    /**
     * Constructor de productos
     * @param n el nombre del producto
     * @param p el precio del producto
     */
    public Producto(String n, double p){
        
        nombre=n;
        precio=p;
    }
    
    /**
     * Metodo get del nombre
     * @return devuelve el valor del nombre del producto
     */
    public String getNombre() {
        
        return nombre;
    }
    /**
     * Metodo set del nombre
     * @param nombre valor pasado para nombre del producto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo get del precio
     * @return devuelve el valor del precio del producto
     */
    public double getPrecio() {
        return precio;
    }
    /**
     * Metodo set del precio
     * @param precio valor pasado para el precio del producto
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
}
