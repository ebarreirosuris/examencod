/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencod;

/**
 * Clase Main que gestiona el uso de la tienda, agrega y muestra productos
 * @author ebarreirosuris
 */
public class ExamenCOD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Tienda t1=new Tienda();
        
        //Comento para poder hacer un commit
        t1.añadir("1a", "Champu",3.5);
        t1.añadir("2a","Pasta de dientes", 2.5);
        t1.añadir("3a","Jabon", 100.5);
        t1.añadir("4a", "Acondicionador", 35.8);
        t1.añadir("5a","Tinte",200.25);
        //Llamamos al metodo mostrar del objeto tienda para que muestre por consola el contenido del TreeMap
        t1.mostrar();
        
    }
}
