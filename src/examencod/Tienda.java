/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencod;

import java.util.Map;
import java.util.TreeMap;

/**
 *La clase tienda es un conjunto de productos almacenados 
 * en un Treemap de forma ordenada por su numero de serie
 * simulando el stock de una tienda.
 * @author ebarreirosuris
 */
public class Tienda {
    
    private Map<String,Producto> tm;
    
    /**
     * Constructor de la clase Tienda, inicializa el TreeMap
     */
    public Tienda(){
        
        tm=new TreeMap();
    }
    /**
     * El metodo añadir se encarga de añadir nuevos productos al TreeMap
     * @param clave recibe el numero de serie del producto
     * @param p recibe un objeto Producto a añadir al stock de la tienda
     */
    public void añadir(String clave,String nombre, double precio){
        
        Producto p=new Producto(nombre,precio);
        tm.put(clave,p);
    }
    
    /**
     * Metodo que muestra por consola el contenido del TreeMap que almacena los productos de la tienda
     */
    public void mostrar(){
        System.out.println("Lista de productos:\n****************************");
        for(Map.Entry entrada : tm.entrySet()){
            
            Producto p=(Producto)entrada.getValue();
            System.out.println(entrada.getKey()+" Nombre: "+p.getNombre()+" |Precio: "+p.getPrecio());
            
        }
    }
    
    
}
